[English](/Rules/ChannelUsage.md) | [Français](/Translated/French/Rules/ChannelUsage.md) | [Español](/Translated/Spanish/Rules/ChannelUsage.md)

---
# Utilizarea canalelor

Aici poți găsi ce scop are fiecare canal de pe [serverul nostru de discord](https://discord.mrcl.cc)

**Dacă vorbești o alta limba decât engleză dă click [aici](#Canale-pentru-alte-limbi) sa găsești canalele acelea.**

# Categoria generala

### \#general
Canalul general este pentru conversatii mai serioase despre topicuri serioase. Postarea "shitpostului" nu este permisă, și memeurile sunt permise decât dacă au legatură cu topicul. Nu sunt reguli grele, doar nu posta memeuri cand alți oameni sunt in mijlocul unei conversatii serioase.
### \#sewers
Sewers este pentru "shitposting" și pentru memeuri, sau pentru linkuri de tipul "Hey această postare de pe reddit este destul de amuzantă". Totuși acest lucru nu înseamnă că poți spama, dar poți fi mai liber în acest canal.
### \#bot-spam
Bot-spam este pentru a te juca cu boții. Acest canal nu va fi monotorizat atat de mult, asa ca sa contactezi un moderator daca oameni incalca regulile. Acestea fiind spuse acest canal poate fii spamat cateodata, așa că regula de nu a spama nu este foarte strictă.
### \#voice-chat
Voice-chat este pentru oamenii care vor sa interacționeze cu persoanele care discută pe canalele de voce prin text. Nu folosi boții de muzică aici, folosește-i in #bot-spam.
### \#suggestions
Acest canal este pentru ați posta sugestile pentru serverul de discord și pentru serverul de minecraft. Ține minte că ideile tale poate ca au fost sugerate deja, și că am putea ignora complet unele dintre ele. Este doar un loc distractiv pentru brainstorming.

# Canale pentru alte limbi
La MRCL suntem deschiși la persoanele care vorbesc alte limbi. Deocamdata avem aceste canale disponibile:
- \#fr-talk
- \#es-talk
- \#fi-talk

Aceste canale sunt ori pentru a vorbii limba respectivă sau pentru ajutor cu acele limbi (ex: "Cum spun la 'baghetă' în franceza?")

Daca vrei un canal pentru limba ta, contactează un admin și o sa o luăm in considerare.

# Categoria cu canale diverse
Nu toate canalele care sunt în această categorie vor fi explicate aici, doar cele cu reguli specifice.

### \#head-area-exposing-channel
Acesta este un canal pentru a face face-revealuri. Nu posta nuduri cu tine și nu posta face-revealuri false (sau cu altcineva). Nu este amuzant.

### \#not-safe-for-kids
**Pornografia si nuditatea nu sunt permise În acest canal**

NSFK este un canal unde poți posta memeurile tale mai Întunecate sau content care nu este foarte adecvat pentru un copil de 14 ani. Aici poți sa discuți despre topicuri mai sensibile precum sexualitatea, politica sau despre sex (masculin,feminin,etc).(Aceste topicuri au voie sa fie discutate si in alte canale doar ca acesta este canalul dedicat)

### \#mrcl-memes
Acest canal este **doar pentru memeuri**. **Nu** răspunde la memeurile postate aici. Acest canal este doar sa archivam toate memeurile legate de MRCL.

### \#free-games
Acest canal este pentru a posta jocuri care sunt gratis pentru o perioadă limitată de timp. Avem un bot care ocazional postează linkuri aici. (Nu suntem responsabili pentru contentul care botul il posteaza)

### \#screen-shots
Acest canal este unde poți posta minunatele tale poze de pe MRCL. **Comentarile nu sunt permise**. Acesta nu este canalul unde poți raporta buguri, acela ar fi #bug-report

Daca îți place o poză, poți reacționa cu emojiul 📌, si îi vom da pin dacă le place poza la mai mulți oameni.

Am putea folosi aceste poze pe siteul nostru si pe alte rețele de socializare.