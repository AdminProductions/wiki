[English](/Rules/ChannelUsage.md) | [Español](/Translated/Spanish/Rules/ChannelUsage.md) | [Romanian](/Translated/Romanian/Rules/ChannelUsage.md)

---
# Utilisation des salons
Ici, vous pouvez trouver à quoi servent nos salons sur notre [serveur Discord](https://discord.mrcl.cc).

**Si vous voulez parler une langue qui n'est pas l'anglais, cliquez [ici](#Salons-pour-les-langues) pour trouver les salons adaptés**

# Catégorie générale
### \#general
Le salon général est fait pour des discussions sérieuses sur des sujets sérieux. Ne faites pas de "Shitposting" et les memes sont seulement autorisés si ils font partis du sujet. On ne force pas sur les messages de ce salon, juste ne postez pas un meme comme ça pendant que des gens ont une discussion sérieuse.

### \#sewers
Le salon "sewers" est fait pour le "shitposting" et les memes ou même pour les liens du genre 'hey, ce post reddit est vraiment cool'. Cela veut pas dire que vous pouvez spam/abussez même si on est moins exigent dans ce salon.

### \#bot-spam
Bot-spam est fait pour entrer les commandes des bots. Ce salon est cassiment jamais sous surveillance, donc si quelqu'un a un comportement inadéquat, veuillez contactez un modérateur. Cela étant dit, ce salon a, en général, beaucoup de 'spam', donc la règle du pas de spam n'est pas valable ici.

### \#voice-chat
Ce salon est utilisé pour ceux qui veulent intéragir avec les personnes dans le vocal mais par texte. N'utilisez pas de bots, faites le dans #bot-spam

### \#suggestions
Ce salon est fait pour poster vos suggestions pour le serveur Discord ou le serveur Minecraft. Gardé à l'esprit que vos idées ont sûrement étaient déjà demandé auparavant et il est possible qu'on en ignore certains. C'est juste un endroit pour s'amuser et discuter des idées.

# Salons pour les langues
Sur MRCL, nous sommes ouverts aux personnes qui parlent d'autres langues. Nous avons des salons pour ses langues pour le moment :
- \#fr-talk
- \#es-talk
- \#fi-talk

Ces salons sont soit pour parler la langue en question, soit pour demander de l'aide dans la langue. (ex: "Comment on dit 'baguette' en espagnol ?")

Si vous voulez un salon pour votre langue, contactez un administrateur et nous verons.

# Catégorie divers
Tout les salons ne seront pas forcément dans la liste, seulement ceux qui ont des règles spécifiques.

### \#head-area-exposing-channel
Ce salon est fait pour faire des 'face reveals' (montrés votre tête en photo). Ne postez pas des nudes de vous (oe même d'une autre personne) et ne postez pas des fausses photos, c'est juste pas drôle. **Ne faites pas le 'face reveal' de quelqu'un d'autre** non plus.

### \#not-safe-for-kids
**Le porno et la nudité n'est pas autorisé dans ce salon.**

NSFK est un salon où vous pouvez mettre des memes ou du contenu qui est, comment dire, pas adaptez pour les personnes de 14 ans ou moins. Vous pouvez aussi parler de sujets un peu plus sensible comme la sexualité, politiques ou les genres. (À noter: ces sujets ne sont pas bannis dans les autres salons, juste que ce salon est plus adapté pour ça.)

### \#mrclmemes
Ce salon est pour les **memes seulement**. 
Ne mettez **pas** de commentaires sur les memes postés. C'est juste un salon d'archives de tout les memes en rapport à MRCL.

### \#free-games
Ce salon est un endroit où vous pouvez mettre les jeux qui sont **gratuits à garder** pendant un temps limité. Nous avons également un bot qui met des messages régulièrement. (Nous sommes pas responsables de ce que met le bot.)

### \#screenshots
Ce salon est fait pour mettre vos belles capture d'écrans d'MRCL. **Les commentaires ne sont pas autorisés.**
C'est également pas un salon pour mettre des bugs, faites ça dans #bug-report.

Si vous aimez une photo, vous pouvez réagir avec la réaction :pushpin: et si il y a assez de monde qui sont d'accord alors le message sera éplingé.

On pourrait utiliser vos photos pour nos réseaux sociaux et le site web.