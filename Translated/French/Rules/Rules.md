[English](/Rules/Rules.md) | [Español](/Translated/Spanish/Rules/rules.md) | [Romanian](/Translated/Romanian/Rules/rules.md)

---
# Règles
Les règles de ce document s'appliquent sur le serveur Discord et le serveur Minecraft.
Ce sont une ligne directrice, se sont les modérateurs et les administrateurs qui feront le choix final.
Elles sont listées dans aucun ordre en particulier sauf la première règle.

## La Première Règle
La première règle, la plus importante, c'est : **soyez logique**.

N'allez pas embêter les autres inutilement, soyez un humain normal. On espère ne pas rajouter une règle car on a surestimé le cerveau humain.

## Règles Globales

- Les décisions de la modération sont définitives. Si vous voulez vous plaindre contactez un administrateur ou contactez Jack si vous voulez portez plainte sur un administrateur.

- **Dire à quelqu'un où se trouve un succès (advancement) ou comment faire un succès n'est pas autorisé.** Cependant, vous pouvez expliquer les mécaniques de bases du jeu. Une liste de tout ce qui est autorisé à expliquer ou non se trouve [ici](/Translated/French/Rules/AdvancementRules.md)

- Le spam/abus n'est pas autorisé. Selon le salon, cette règle est appliquée de façon différente. Allez voir l'[Utilisation des salons](/Translated/French/Rules/ChannelUsage.md) pour plus de détails.

- Vous pouvez débattre sur presque tout, cependant nous intervenons si il y a des insultes envers les personnes. Si vous voulez vous réglez vos problèmes, allez en MP.

- Le racisme et la discrimination est en aucun cas acceptés.

- On ne banni pas pour des insultes tant que celle-ci ne dépasse pas la limite des insultes personnelles.

- Vous pouvez **seulement** faire de la pub de façon raisonnable pour des projets en rapport avec Code Lyoko. Ce sont les modérateurs qui décide si ça passe. Si vous voulez faire de la pub pour un autre projet hors Code Lyoko, demandez à un adminisatrateur.

- **Nous avons des restrictions sur les pseudos et les surnoms, vous pouvez les lire [ici](Translated/French/Rules/NamingRules.md)**

- La langue la plus utilisée est l'Anglais mais nous avons une communauté qui parle d'autres langues, que ça soit en jeu ou dans les [salons spécifiques](Translated/French/Rules/ChannelUsage.md#Language_channels)

- Les double comptes ne sont pas autorisés.

- Les contenus du type "NSFW" comme la nudité ou du porno ne sont pas autorisés. Les memes et les blagues sont acceptés à un certain degré dans les [salons](ChannelUsage.md) appropriés.

- Si vous essayez d'échapper à ban, alors vous serez banni à nouveau. Pour toujours.

- **Embêter les autres personnes en MP ou sur les autres serveurs peut valoir un ban sur notre serveur.**

## Règles spécifiques à Discord

- Mentionner une personne ou un rôle excessiment sans raison sera suivi d'un mute, voir pire.

- Troller/Embêter les gens avec des bots n'est pas autorisé. Si quelqu'un se plaint, vous serez en tort.

## Règles spécifiques à Minecraft

- Troller en utilisant les mécaniques de jeu n'est pas autorisé. Quelques exemples, qui ne représente pas la liste complète : 
    * Changer la destination du scanner d'une personne sans demander,
    * Abuser du Retour vers le passé,
    * Combattre quelqu'un en boucle qui ne veut pas se battre et qui est ni xanatifié.