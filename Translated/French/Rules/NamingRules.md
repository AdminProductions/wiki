[English](/Rules/NamingRules.md) | [Español](/Translated/Spanish/Rules/NamingRules.md) | [Romanian](/Translated/Romanian/Rules/NamingRules.md)

---
# Règles sur les pseudos
Nous avons des règles précises pour les pseudos que ça soit sur Minecraft ou Discord.

## Les pseudos qui ne sont pas autorisés
Vous pouvez pas avoir un pseudo qui :
* Ressemble à un personnage de la série
* Contient des caractères spéciaux difficilement reconnaisables
* Ressemble à pas grand chose (voir exemple en dessous)
* Est difficilement mentionnable sur Discord

## Qu'est-ce que je dois faire si j'ai un nom banni
Ça dépend. Voici quelques scénarios :

### Le nom banni est votre pseudo Minecraft
Si vous avez un compte légal/premium de Minecraft et que vous voulez pas changer votre pseudo via mojang.com, demandez à un modérateur de changer votre surnom sur le jeu.

**Si vous avez pas acheté Minecraft, changer votre pseudo sur le launcher ou vous *allez* être banni**

### Le nom banni est votre pseudo Discord
Si vous voulez pas changer votre pseudo Discord, utilisez la fonction pour se renommer sur le serveur uniquement (ou on choisira un pseudo pour vous et vous allez pas l'aimer).

## Exemples de pseudos banni
### 11ε ƉơƈŧēůƦ
Pas accepté car il y a trop de caractères spéciaux.
### Aelita_Shaeffer
Pas accepté car c'est un personnage de la série.
### Jeremie
Pareil que le cas au dessus. Si votre nom est vraiment Jeremie, demander à un modérateur.
### gksqzeoKKSdqslAZRf
Pas accepté car ce pseudo ne peut pas être utiliser comme nom. On va pas dire à chaque fois : "Salut gksqzeoKKSdqslAZRf, comment ça va ?".